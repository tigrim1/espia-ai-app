import { getEmployee, singInUser, getPost } from "../services/auth";
import {getUser} from "../components/User/user";
const me = getUser()
export async function genartePost(){
  const post = getPost()
  console.log(post)
  return post
}
const posts = [
  {
    id: 'p1',
    user: me,
    createdAt: '2022-01-31T12:00:00.000Z',
    content: 'Conteudo do Post',
    image: require('../images/sala-1.png')
  },
  {
    id: 'p2',
    user: {
      id: 'user2',
      username: 'username2',
      name: 'Name2',
      image: require('../images/fotodouser.png'),
      school:{
        name:"Eetepa Dr. Celso Malcher"
      }
    },
    createdAt: '2022-01-31T12:00:00.000Z',
    content: 'Conteudo do Post 2',
    image: require('../images/sala-3.png')
  },
  {
    id: 'p3',
    user: {
      id: 'user2',
      username: 'username2',
      name: 'Name2',
      image: require('../images/fotodouser.png'),
      school:{
        name:"Eetepa Dr. Celso Malcher"
      }
    },
    createdAt: '2022-01-31T12:00:00.000Z',
    content: 'Conteudo do Post 3',
  }
]

export default posts
