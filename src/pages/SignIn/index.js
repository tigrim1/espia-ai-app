import React from 'react';
import { Input, Center,  Button, VStack, Icon, IconButton, FormControl } from "native-base";
import EetepaLogo from '../../components/EetepaLogo';
import { getEmployee } from "../../services/auth";
import {useAuth} from "../../contexts/auth";
import FormControlErrorMessage from "native-base/src/components/composites/FormControl/FormControlErrorMessage";


function SignIn({navigation}) {
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);
  const context = useAuth()

  const [registration, setRegistration] = React.useState('');
  const [password, setPassword] = React.useState('');

  const [invalid, setInvalid] = React.useState(false);
  const [registrationMessageErro, setRegistrationMessageError] = React.useState('');
  const [passwordMessageErro, setPasswordlMessageErro] = React.useState('');


  async function handleSignIn() {
    if(registration === ''){
      setInvalid(true);
      setRegistrationMessageError('Preencha o campo de Matrícula');
      if(password === ''){
        setPasswordlMessageErro('Preencha o campo senha');
      }
    }else{
      if(password === ''){
        setInvalid(true);
        setRegistrationMessageError('');
        setPasswordlMessageErro('Preencha o campo senha')
      }else{
        setInvalid(false);
        const id = registration[0]
        const employee = await getEmployee(id)
        context.setUserAuth(employee)
        if(employee !== 404){
          navigation.navigate('Feed');
        }
        else{
          setInvalid(true);
          setPasswordlMessageErro('Informações não encontradas')
        }
      }
    }
  }

  return (
    <Center flex={1}>
      <VStack space="20">
        <Center>
          <EetepaLogo />
        </Center>
        <VStack space={3}>
          <FormControl isRequired isInvalid={invalid}>
            <FormControl.Label>Matrícula do Funcionário</FormControl.Label>
            <Input onChangeText={value => setRegistration(value)}/>
            <FormControlErrorMessage>{registrationMessageErro}</FormControlErrorMessage>
            <FormControl.Label>Senha</FormControl.Label>
            <Input type={'password'} onChangeText={value => setPassword(value)}/>
            <FormControlErrorMessage>{passwordMessageErro}</FormControlErrorMessage>
          </FormControl>
        </VStack>
        <VStack space={5}>
          <Button onPress={handleSignIn}>Logar</Button>
          <Button onPress={() => navigation.goBack()}>Voltar</Button>
        </VStack>
      </VStack>
    </Center>
  );
}

export default SignIn;

//Todo: Autenticação e tratamento de login
