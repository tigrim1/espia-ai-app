import React, { useEffect, useState } from "react";
import { Center, Box, Button, ScrollView, VStack, Image, View, Text, HStack, Heading, Spinner } from "native-base";
import ToggleDrawer from "../../components/ToggleDrawer";
import Post from "../../components/Post";
import { NativeBaseProvider } from "native-base/src/core/NativeBaseProvider";
import { getEmployee, getPost, getPosts } from "../../services/auth";

//Todo: Ordenar o feed, usar a função lá da hierarquia
const Feed: React.FC = ({navigation}) => {

  const [feed, setFeed] = useState([])
  const [loading, setLoading] = React.useState(true)

  useEffect(() => {
    async function loadpage(){
      const posts = await getPosts()
      const feedItens = []

      for(let i = 0; i < posts.length; i ++){
        const post = await getPost(posts[i].id)
        const userPost = await getEmployee(post.employee_id)
        feedItens.push(
          <Post PostProps={post} EmployeeProps={userPost} key={post.id}/>
          )
      }
      setFeed(feedItens)
      setLoading(false)
    }
    loadpage()
  }, [])


  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }
  return (
    <View bg={'#f0f2f5'}>
      <ScrollView>
        {feed}
      </ScrollView>
    </View>


  );
};

export default Feed;
