import React from "react";
import {
  VStack,
  View,
  Text,
  HStack,
  Pressable, Button,
} from "native-base";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { useAuth } from "../../contexts/auth";
import ProfilePicture from "../../components/ProfilePicture";

//Todo: Adicionar função de singout apropriadamente

const Personal: React.FC = ({navigation}) => {
  const context = useAuth()
  const user = context.user

  function signOut(){
    navigation.navigate('Choose User')
  }

  return (
    <View>
      <VStack space={20} alignItems={'center'} marginTop={10}>
        <View alignItems={'center'} justifyContent={'center'}>
          <ProfilePicture image={user.photo} size={75}/>
          <Text fontSize={'2xl'}>{user.name}</Text>
          <Text fontSize={'xs'}>{user.school.name}</Text>
        </View>
        <VStack space={2}>
          <Pressable>
            <HStack space={2} alignItems={'center'} >
              <FontAwesome5 name={'user-edit'} size={15}/>
              <Text>Editar Informações Pessoais</Text>
            </HStack>
          </Pressable>
          <Pressable>
            <HStack space={2} alignItems={'center'} >
              <FontAwesome5 name={'id-card-alt'} size={15}/>
              <Text>Vizualizar Perfil</Text>
            </HStack>
          </Pressable>
        </VStack>

      </VStack>
    </View>
  );
};

export default Personal;
