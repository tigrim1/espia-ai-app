import React, { useEffect } from "react";
import {
  Center,
  Box,
  Button,
  Pressable,
  HStack,
  Badge,
  Text,
  Flex,
  Heading,
  VStack,
  ScrollView,
  View, Spinner,
} from "native-base";
import { Spacer } from "native-base/src/components/primitives/Flex";
import ToggleDrawer from "../../components/ToggleDrawer";
import { getEmployee, getInstitutions, getSubject, getSubjectClass, getSubjectClassById } from "../../services/auth";
import { NativeBaseProvider } from "native-base/src/core/NativeBaseProvider";
import Subject from "../../components/Subject";
import { useAuth } from "../../contexts/auth";

//Todo: Alunos tem que ver só as materias que estão participando
const Classes: React.FC = ({navigation}) => {
  const user = useAuth().user
  const [classScreen, setClassScreen] = React.useState(null)
  const [loading, setLoading] = React.useState(true)

  useEffect(()=>{
    async function attPage(){
      const subjects = await getSubjectClass()
      const subjectList = []
      for (const subject in subjects) {
        if(subjects[subject].employee_id === user.id){
          subjectList.push( await getSubjectClassById(subjects[subject].id))
        }
      }
      const component = []
      for (const classKey in subjectList) {
        component.push(
          <Subject SubjectProps={subjectList[classKey]} navigation={navigation} key={subjectList[classKey].id}/>
        )
      }
      setClassScreen(component)
      setLoading(false)
    }
    attPage()
  }, [])

  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }
  if(!user.is_teacher){
    return(
      <Center alignItems={'center'}>
        <Text>Você não é professor</Text>
      </Center>
    )
  }
  return (
    <View>
      <ScrollView>
        <Heading marginLeft={2} margin={'5px'}>Turmas</Heading>
        <VStack space={2}>
          {classScreen}
        </VStack>
      </ScrollView>
    </View>

  );
};

export default Classes;
