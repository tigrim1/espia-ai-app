import React, { useEffect } from "react";
import {Center, Button, VStack, HStack} from "native-base";
import {useAuth} from '../../contexts/auth';
import EetepaLogo from "../../components/EetepaLogo";
import { signInApi } from "../../services/auth";

const ChooseSignIn: React.FC = ({navigation}) => {
  return (
    <Center flex={1}>
      <VStack space={'20'} alignItems={'center'} justifyContent={'center'}>
        <EetepaLogo/>
        <VStack space={5}>
          <Button size={"lg"} padding={'5'} onPress={() => navigation.navigate('Login')}>Funcionário</Button>
          <Button size={"lg"} padding={'5'} onPress={() => navigation.navigate('SignInStudent')}>Aluno</Button>
          <Button size={"lg"} padding={'5'} onPress={() => navigation.navigate('FeedNoSignIn')}>Continuar sem logar</Button>
        </VStack>
      </VStack>
    </Center>
  );
};

export default ChooseSignIn;
