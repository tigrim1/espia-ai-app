import React from 'react';
import {Center, Button, VStack, HStack} from "native-base";
import EetepaLogo from "../../components/EetepaLogo";

const DashboardNoSignIn: React.FC = ({navigation}) => {

  return (
    <Center flex={1}>
      <VStack space={'20'} alignItems={'center'} justifyContent={'center'}>
        <EetepaLogo/>
        <HStack space={5}>
          <Button size={"lg"} padding={'7'} onPress={() => navigation.navigate('Feed')}>Noticias</Button>
          <Button size={"lg"} padding={'7'} onPress={() => navigation.navigate('Institution')}>Instituição</Button>
        </HStack>
        <Button onPress={() => navigation.goBack()}>Voltar</Button>
      </VStack>
    </Center>
  );
};

export default DashboardNoSignIn;
