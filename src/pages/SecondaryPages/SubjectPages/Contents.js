import React, { useEffect } from "react";
import { Center, ScrollView, Spinner, Text, VStack } from "native-base";
import { NativeBaseProvider } from "native-base/src/core/NativeBaseProvider";
import { getSubjectClass } from "../../../contexts/subjects";
import { getContentById } from "../../../services/auth";
import Content from "../../../components/Content";

const Contets = () => {
  const subject = getSubjectClass()
  const [screen, setScreen] = React.useState([])
  const [loading, setLoading] = React.useState(true)

  useEffect(() => {
    async function loadpage(){
      const contents = []
      const itens = []
      for (const content in subject.contents) {
        contents.push(await getContentById(subject.contents[content].id))
      }

      for (const contentsKey in contents) {
        itens.push(
          <Content ContentProps={contents[contentsKey]} key={contents[contentsKey].id}/>
        )
      }

      setScreen(itens)
      setLoading(false)
    }
    loadpage()
  }, [])


  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }
  return(
    <ScrollView>
      {screen}
    </ScrollView>
  )

}

export default Contets
