import React, { useEffect } from "react";
import { Box, Center, Heading, HStack, ScrollView, Spinner, Text, View, VStack } from "native-base";
import { getSudentClassById } from "../../../services/auth";
import { NativeBaseProvider } from "native-base/src/core/NativeBaseProvider";
import { getSubjectClass } from "../../../contexts/subjects";
import ProfilePicture from "../../../components/ProfilePicture";
import { FlatList } from "react-native";
import { Spacer } from "native-base/src/components/primitives/Flex";

const Participants = () => {
  const subject = getSubjectClass()
  const [teacher, setTeacher] = React.useState(null)
  const [students, setStudents] = React.useState(null)
  const [loading, setLoading] = React.useState(true)

  useEffect(() => {
    async function loadpage(){
      const studentList = []
      const studentClass = await getSudentClassById(subject.student_class.id)

      for (const students in studentClass.students) {
        studentList.push(
          studentClass.students[students]
        )
      }

      setTeacher(subject.employee)
      setStudents(studentList)
      setLoading(false)
    }
    loadpage()
  }, [])


  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }
  return(
    <View marginTop={5} marginLeft={3}>
      <VStack space={2}>
        <Text fontSize={'xl'}>Professor</Text>
        <HStack space={4} alignItems={'center'}>
          <ProfilePicture image={teacher.photo} size={45}/>
          <Text  color="coolGray.800" bold>{teacher.name}</Text>
        </HStack>
        <Text fontSize={'xl'}>Alunos</Text>
        <FlatList data={students} renderItem={({
                                             item
                                           }) => <Box borderBottomWidth="1"borderColor="gray.200" padding={1} maxW={'95%'}>
          <HStack space={3} alignItems={'center'}>
            <ProfilePicture image={item.photo} size={45}/>
              <Text color="coolGray.800" bold>
                {item.name}
              </Text>
            <Spacer />
          </HStack>
        </Box>} keyExtractor={item => item.id} />
      </VStack>
    </View>

  )

}

export default Participants
//Todo: Ajeitar os Frontend dessa desgraça
