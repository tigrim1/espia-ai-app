import React, { useEffect } from "react";
import { Box, Divider, Image, Text, View, VStack } from "native-base";
import { getSchool } from "../../../contexts/institution";
import api from "../../../services/api";

//Todo: Adicionar o google maps
const Location = () => {
  const baseUrl = api.defaults.baseURL+"/storage/"
  const school = getSchool()
  const scolarAreas = []
  for (const areas in school.school_areas) {
    scolarAreas.push(
      <VStack key={school.school_areas[areas].id} >
        <Text fontSize={'lg'}>{school.school_areas[areas].name}</Text>
        <Text color={'gray.400'} fontSize={'xs'}>{school.school_areas[areas].description}</Text>
        <Divider />
      </VStack>
    )
  }
  return(
    <View padding={5} bg={'#f0f2f5'}>
      <Box bg={'white'} padding={3} rounded={20} margin={'5px'} maxW={'98%'}>
        <Text fontSize={'2xl'}>{school.name}</Text>
        <VStack space={1}>
          <Text color={'gray.400'}>{school.public_place} n°{school.public_place_number}</Text>
        </VStack>
      <Divider/>
      <Image margin={5} source={{uri: baseUrl+school.photo}} alt={'Foto da escola'} height={200} resizeMode={'cover'}/>
      </Box>
      <Box bg={'white'} padding={3} rounded={20} maxW={'98%'}>
        <Text fontSize={'2xl'} >Áreas</Text>
        {scolarAreas}
      </Box>

    </View>

  )

}

export default Location
