import React, { useEffect } from "react";
import { Box, Center, HStack, ScrollView, Spinner, Text, VStack } from "native-base";
import { getSchool } from "../../../contexts/institution";
import { getGroups, getRoles } from "../../../services/auth";
import { NativeBaseProvider } from "native-base/src/core/NativeBaseProvider";
import ProfilePicture from "../../../components/ProfilePicture";
import { Spacer } from "native-base/src/components/primitives/Flex";

//Todo: Frontend dessa tela
function sortHierachy(list){
  list.sort((a,b) => {
    if (a.hierarchy > b.hierarchy) {
      return 1;
    }
    if (a.hierarchy < b.hierarchy) {
      return -1;
    }
    // a must be equal to b
    return 0;
  })

  return list
}

const ManagementTeam = () => {
  const school = getSchool()
  const [team, setTeam] = React.useState([])
  const [loading, setLoading] = React.useState(true)

  useEffect(() => {
    async function loadpage(){
      const groupItens = []
      const groups = await getGroups()
      const orderedGroups = sortHierachy(groups)
      const employees = school.employees
      const roles = await getRoles()
      const orderedRoles = sortHierachy(roles)

      for (const group in orderedGroups) {
        groupItens.push(
          <Text fontSize={'2xl'} key={orderedGroups[group].name}>{orderedGroups[group].name}</Text>
        )
        for (const role in orderedRoles) {
          for (const employee in employees) {
            if(employees[employee].role_id === orderedRoles[role].id && orderedRoles[role].group_id === orderedGroups[group].id){
              groupItens.push(
                <Box borderBottomWidth="1"borderColor="gray.200" padding={1} maxW={'95%'} key={employees[employee].id}>
                  <Text fontSize={'lg'}>{employees[employee].name}</Text>
                  <Text fontSize={'xs'} color={'gray.500'}>{orderedRoles[role].name}</Text>
                    <Spacer />
                </Box>
              )
            }
          }
        }
      }
      setTeam(groupItens)
      setLoading(false)
    }
    loadpage()
  }, [])


  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }
  return(
    <ScrollView padding={5} bg={'#f0f2f5'}>
      {team}
    </ScrollView>
  )

}

export default ManagementTeam
