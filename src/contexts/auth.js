import React, {createContext, useState, useEffect, useContext} from 'react';
import {Center, Spinner} from 'native-base';
import * as auth from '../services/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NativeBaseProvider} from 'native-base/src/core/NativeBaseProvider';
import api from '../services/api';
import { signInApi } from "../services/auth";

const AuthContext = createContext({});

export const AuthProvider: React.FC = ({children}) => {
  const [registration, setRegistration] = useState('')
  const [password, setPassword] = useState('')
  const [user, setUser] = useState(null)

  const [token, setToken] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadStorageData() {
      const storageToken = await AsyncStorage.getItem('@EPAuth:token');

      if (storageToken) {

        api.defaults.headers.Authorization = `Bearer ${storageToken}`;
        setToken(storageToken);
        setLoading(false);
      }
      else{

        await signInAPI();
      }

    }
    loadStorageData();

  }, []);

  async function signInAPI(){
    const token = await auth.signInApi();
    api.defaults.headers.Authorization = `Bearer ${token}`;
    await AsyncStorage.setItem('@EPAuth:token', token);
    console.log("Novo Token:", token)
    setToken(token)
    setLoading(false)
  }


   function setUserAuth(user) {
    setUser(user)
  }

  async function signInStudent() {


  }


  function signOut() {
    AsyncStorage.clear().then(() => {
      setToken(null);
    });
  }

  if (loading) {
    return (
      <NativeBaseProvider>
        <Center flex={1}>
          <Spinner size="lg" accessibilityLabel={'Loading'}>
            Loading
          </Spinner>
        </Center>
      </NativeBaseProvider>
    );
  }

  return (
    <AuthContext.Provider value={{user, setUserAuth, setLoading, token}}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);

  return context;
}
