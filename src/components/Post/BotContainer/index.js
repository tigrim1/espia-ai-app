import React from "react";
import { Image, Text, View } from "native-base";
import ProfilePicture from "../../ProfilePicture";
import api from "../../../services/api";

const baseUrl =  api.defaults.baseURL+"/storage/"
const BotContainer = ({post}) => {
  return(
    <View>
      <Text marginBottom={2}>{post.description}</Text>
      {!!post.photo && <Image source={{uri: baseUrl+post.photo}} alt={'optinal image'}  height={200} resizeMode={'cover'}/>}
    </View>
  )
}

export default BotContainer
