import React from "react";
import { Box, Pressable, Text, VStack } from "native-base";
import { useNavigation } from "@react-navigation/native";

//Todo: Fazer abrir uma outra pagina ou pdf
//Todo: Uma pagina pra uma descrição detalhada da atividade

const Content = ({ContentProps}) => {
  const navigation = useNavigation()
  const date = ContentProps.date.split('-')

  function onEvent(){
    navigation.navigate('TaskScreen', {ContentProps: ContentProps})
  }

  return(
    <Pressable  margin={'5px'} onPress={onEvent}>
      <Box padding={2} borderWidth={'2px'} rounded={'8'}  borderColor={'#dbdbdb'} bg={'white'}>
        <VStack space={2}>
          <Text fontSize={'2xl'}>{ContentProps.name}</Text>
          <Text>{date[2]}/{date[1]}/{date[0]}</Text>
        </VStack>
      </Box>
    </Pressable>
  )
}

export default Content
