import React from "react";
import { Box, HStack, Pressable, Text, VStack } from "native-base";
import ProfilePicture from "../ProfilePicture";

const Participant = ({ParticipantProps}) => {

  return(
    <HStack space={5} alignItems={'center'}>
      <ProfilePicture image={ParticipantProps.photo} size={45}/>
      <Text fontSize={'xl'}>{ParticipantProps.name}</Text>
    </HStack>
  )
}

export default Participant
