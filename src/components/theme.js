import React from "react";
import {extendTheme} from "native-base";


export const theme = extendTheme({
  colors: {
    // Add new color
    primary: {
      50: '#ee4444',
      100: '#ab06FF',

    },
    // Redefinig only one shade, rest of the color will remain same.
    amber: {
      400: '#d97706',
    },
  },
  config: {
    // Changing initialColorMode to 'dark'
    initialColorMode: 'light',
  },
  components: {
    Button: {
      defaultProps: {
        colorScheme: `red`,

      }
    },

    IconButton: {
      defaultProps: {
        colorScheme: 'gray',
      }
    }

  },
});

