import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Dashboard from '../pages/DashBoard';
import DashboardNoSignIn from "../pages/DashboardNoSignIn";

const AppStack = createNativeStackNavigator();

export const AppRoutes = () => (
  <AppStack.Navigator screenOptions={{headerShown: false}}>
    <AppStack.Screen name={'Dashboard'} component={Dashboard} />
  </AppStack.Navigator>
);

export const NoAppRoutes = () => (
  <AppStack.Navigator screenOptions={{headerShown: false}}>
    <AppStack.Screen name={'DashboardNoSignIn'} component={DashboardNoSignIn} />
  </AppStack.Navigator>
);

//export default AppRoutes;
