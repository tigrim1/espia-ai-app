import React from 'react';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {useAuth} from '../contexts/auth';

import {AppRoutes, NoAppRoutes} from './app.routes';
import {AuthRoutes, ChooseUserRoutes} from './auth.routes';

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {createMaterialBottomTabNavigator} from "@react-navigation/material-bottom-tabs";
import { createDrawerNavigator } from '@react-navigation/drawer';


import Dashboard from "../pages/DashBoard";
import DashboardNoSignIn from "../pages/DashboardNoSignIn";
import SignIn from "../pages/SignIn";
import ChooseSignIn from "../pages/ChooseSignIn";
import SignInStudent from "../pages/SignInStudent";
import Feed from "../pages/Feed";
import Classes from "../pages/Classes";
import Institution from "../pages/Institution";
import MakePost from "../pages/Feed/MakePost";
import Personal from "../pages/Feed/Personal";
import FeedNoSignIn from "../pages/FeedNoSignIn";
import Location from "../pages/SecondaryPages/InstitutionPages/Location"
import ManagementTeam from "../pages/SecondaryPages/InstitutionPages/ManagementTeam";
import Contents from "../pages/SecondaryPages/SubjectPages/Contents";
import Participants from "../pages/SecondaryPages/SubjectPages/Participants";
import TaskScreen from "../pages/SecondaryPages/SubjectPages/TaskScreen";
import { DrawerContent } from "./DrawerContent";
import { Heading } from "native-base";

//Todo: Trocar todos os icones pra deixar padrão e bonitin

const AppStack = createNativeStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const TabLeft = createDrawerNavigator();

export const mainTab = () => {
    return(
      <TabLeft.Navigator screenOptions={{headerTitle: () => <Heading/>}} drawerContent={props => <DrawerContent{...props} />} >
        <TabLeft.Screen name={'FeedLeft'} component={feedTabs} />
        <TabLeft.Screen name={'Classes'} component={Classes}  />
        <TabLeft.Screen name={'Personal'} component={Personal} />
        <TabLeft.Screen name={'Institutions'} component={Institution}/>
      </TabLeft.Navigator>
    )
  }
export const studentTab = () => {
  return(
    <TabLeft.Navigator screenOptions={{headerShown:false}}>
      <TabLeft.Screen name={'FeedLeft'} component={studantTabs}/>
      <TabLeft.Screen name={'Classes'} component={Classes} />
      <TabLeft.Screen name={'Personal'} component={Personal} />
      <TabLeft.Screen name={'Institutions'} component={Institution}/>
    </TabLeft.Navigator>
  )
}
const feedTabsNoSignIn = () => {
  return(
    <Tab.Navigator activeColor={'#ffffff'} inactiveColor={'#730000'} barStyle={{backgroundColor:'#ee4444'}}>
      <Tab.Screen
        name={'FeedBar'}
        component={FeedNoSignIn}
        options={{
          tabBarLabel: 'Notícias',
          tabBarIcon: ({color}) => (
            <Entypo color={color} name="news" size={24} />
          )
        }}
      />
      <Tab.Screen
        name={'Institutions'}
        component={Institution}
        options={{
          tabBarLabel: 'Instituições',
          tabBarIcon: ({color}) => (
            <FontAwesome5 color={color} name={'school'} size={24}/>
          )
        }}
      />
    </Tab.Navigator>
  );
};

const feedTabs = () => {
  return(
    <Tab.Navigator activeColor={'#ffffff'} inactiveColor={'#730000'} barStyle={{backgroundColor:'#ee4444'}}>
      <Tab.Screen
        name={'FeedBarBottom'}
        component={Feed}
        options={{
          tabBarLabel: 'Notícias',
          tabBarIcon: ({color}) => (
            <Entypo name="news" color={color} size={24} />
          )
        }}
      />
      <Tab.Screen
        name={'MakePost'}
        component={MakePost}
        options={{
          tabBarLabel: 'Novo Post',
          tabBarIcon: ({color}) => (
            <AntDesign name={'plussquare'} color={color} size={24}/>
          )
        }}
      />
    </Tab.Navigator>
  );
};

const studantTabs = () => {
  return(
    <Tab.Navigator labeled={false} barStyle={{backgroundColor:'#ee4444'}}>
      <Tab.Screen
        name={'FeedBarBottom'}
        component={Feed}
        options={{
          tabBarLabel: 'Notícias',
          tabBarIcon: () => (
            <Entypo name="news" size={24} />
          )
        }}
      />
      <Tab.Screen
        name={'Personal'}
        component={Personal}
        options={{
          tabBarLabel: 'Perfil',
          tabBarIcon: () => (
            <Entypo name={'user'} size={24}/>
          )
        }}
      />
    </Tab.Navigator>
  );
};

const institutionTabs = () => {
  return(
    <Tab.Navigator activeColor={'#ffffff'} inactiveColor={'#730000'} barStyle={{backgroundColor:'#ee4444'}}>
      <Tab.Screen
        name={'Location'}
        component={Location}
        options={{
          tabBarLabel: 'Informações',
          tabBarIcon: ({color}) => (
            <Entypo color={color} name="news" size={24} />
          )
        }}
      />
      <Tab.Screen
        name={'ManagementTeam'}
        component={ManagementTeam}
        options={{
          tabBarLabel: 'Equipe Gestora',
          tabBarIcon: ({color}) => (
            <AntDesign color={color} name={'book'} size={24}/>
          )
        }}
      />
    </Tab.Navigator>
  );
}

const subjectTabs = () => {
  return(
    <Tab.Navigator activeColor={'#ffffff'} inactiveColor={'#730000'} barStyle={{backgroundColor:'#ee4444'}}>
      <Tab.Screen
        name={'Contents'}
        component={Contents}
        options={{
          tabBarLabel: 'Conteúdo',
          tabBarIcon: ({color}) => (
            <FontAwesome5 color={color} name={'folder'} size={20} />
          )
        }}
      />
      <Tab.Screen
        name={'Participants'}
        component={Participants}
        options={{
          tabBarLabel: 'Participantes',
          tabBarIcon: ({color}) => (
            <FontAwesome5 color={color} name={'user-friends'} size={20} />
          )
        }}
      />
    </Tab.Navigator>
  );
}

const Routes: React.FC = () => {

  return (
    <AppStack.Navigator initialRouteName={'Choose User'} screenOptions={{headerShown:false}}>
      <AppStack.Screen  name={'Choose User'} component={ChooseSignIn} />
      <AppStack.Screen  name={'Login'} component={SignIn} />
      <AppStack.Screen  name={'SignInStudent'} component={SignInStudent} />
      <AppStack.Screen  name={'DashBoard'} component={Dashboard} />
      <AppStack.Screen  name={'DashBoardNoSignIn'} component={DashboardNoSignIn} />

      <AppStack.Screen  name={'Feed'} component={mainTab} />
      <AppStack.Screen  name={'FeedStudent'} component={studentTab} />
      <AppStack.Screen  name={'FeedNoSignIn'} component={feedTabsNoSignIn} />
      <AppStack.Screen  name={'InstitutionPages'} component={institutionTabs} />
      <AppStack.Screen  name={'SubjectPages'} component={subjectTabs} />
      <AppStack.Screen  name={'TaskScreen'} component={TaskScreen} />
    </AppStack.Navigator>

  );
};

export default Routes;
