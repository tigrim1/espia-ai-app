import React from "react";
import { HStack, Text, View, VStack } from "native-base";
import {DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import DrawerSection from "react-native-paper/src/components/Drawer/DrawerSection";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { useAuth } from "../contexts/auth";
import ProfilePicture from "../components/ProfilePicture";


export function DrawerContent(props) {
  const user = useAuth().user
  return(
    <View flex={1}>
      <DrawerContentScrollView {...props}>
        <View>
          <View paddingLeft={'10px'} marginTop={5} borderBottomWidth="1"borderColor="gray.100">
            <ProfilePicture image={user.photo} size={55} />
            <Text fontSize={'xl'}>{user.name}</Text>
            <Text fontSize={'sm'}>{user.role.name}</Text>
          </View >
        </View>
        <DrawerSection style={{marginTop:2}}>
          <DrawerItem
            icon={({color, size}) => (
              <FontAwesome5
                name='home'
                color={color}
                size={size}
              />
            )}
            label={'Notícias'}
            onPress={() => {props.navigation.navigate('FeedBarBottom')}}
          />
          {user.is_teacher === 1 &&
            <DrawerItem
            icon={({color, size}) => (
              <FontAwesome5
                name='book-open'
                color={color}
                size={size}
              />
            )}
            label={'Turmas'}
            onPress={() => {props.navigation.navigate('Classes')}}
          />}
          <DrawerItem
            icon={({color, size}) => (
              <FontAwesome5
                name='school'
                color={color}
                size={size}
              />
            )}
            label={'Instituições'}
            onPress={() => {props.navigation.navigate('Institutions')}}
          />
        </DrawerSection>
      </DrawerContentScrollView>
      <DrawerSection>
        <DrawerItem
          icon={({color, size}) => (
            <FontAwesome5
              name='sign-in-alt'
              color={color}
              size={size}
            />
          )}
          label={'Sair'}
          onPress={() => {props.navigation.navigate('Choose User')}}
        />
      </DrawerSection>
    </View>
  )
}
