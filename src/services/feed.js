import api from "./api";

const createFormData = (photo, body = {}) => {
  const data = new FormData();

  data.append('file', {
    name: photo.fileName,
    type: photo.type,
  });
  Object.keys(body).forEach((key) => {
    data.append(key, body[key]);
  });

  return data;
};

export function makePost(postProps, image){
  //console.log(typeof (image))
  let formData = new FormData()
  formData.append('photo', image)
  formData.append('employee_id', postProps.employee_id)
  formData.append('description', postProps.description)
  formData.append('date', postProps.date)
  console.log(formData)
  // const body = createFormData(image, {
  //   employee_id: postProps.employee_id,
  //   description: postProps.description,
  //   date: postProps.date
  // })
  // console.log(body)
  // api.post(`/api/post`, body , {headers: { "Content-Type": "multipart/form-data" } })
  //   .then((response) => response.json())
  //   .then((response) => {
  //     console.log('response', response);
  //   })
  //   .catch(function (error){
  //     console.log(JSON.stringify(error))
  //   })
  // const promise = api.post('/api/post', body)
  // const data = promise.then((response) => response.data)
  //   .catch((error => error.toJSON().status));
  // return data
}
