import api from '../services/api';

export function signInApi() {
    const promise = api.post('/api/auth/login', {
    email: 'iury@gmail.com',
    password: '123456',
    device_name: 'rafeu-cell'
  })
    const data = promise.then((response) => response.data)
      .catch((error => error.toJSON().status));
  return data
}

export function signOut() {
  //poapoepo
}

export function singInUser(){
  const data = {
    id: 'user1',
    matricula: '2018',
    name: 'Nome 1',
    username: 'username 1',
    institution: 'Eetepa Dr. Celso Malcher',
    image: require('../images/fotodouser.png')
  }
  return data
}

export function singInEmployer(registration, passoword){
  const data = api.get('/api/employee/')
  return data
}

export function getEmployee(id){
  const employee = api.get(`/api/employee/${id}`)

  const data = employee.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function singInStudent(registration, passoword){
  const reg = '123'
  const pass = '123'
  if(registration === reg && passoword === pass){
    return 'token'
  }
  else{
    return false
  }
}

export function getRoles(){
  const roles = api.get(`/api/role`)

  const data = roles.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getGroups(){
  const groups = api.get(`/api/group`)

  const data = groups.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getGroupByID(id){
  const group = api.get(`/api/group/${id}`)

  const data = group.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getInstitutions(){
  const institutions = api.get('/api/school')

  const data = institutions.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}
export function getInstitutionById(id){
  const institutions = api.get(`/api/school/${id}`)

  const data = institutions.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getSubjectClass(){
  const subjectClasses = api.get(`/api/subject_class`)

  const data = subjectClasses.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getSubjectClassById(id){
  const subjectClass = api.get(`/api/subject_class/${id}`)

  const data = subjectClass.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getContentById(id){
  const content = api.get(`/api/content/${id}`)

  const data = content.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}

export function getSudentClassById(id){
  const students = api.get(`/api/student_class/${id}`)

  const data = students.then((response) => response.data)
    .catch((error => error.toJSON().status));
  return data
}



export function getPost(id){
  const post = api.get(`/api/post/${id}`)

  const data = post.then((response) => response.data)
    .catch((error => error.toJSON().status))

  return data
}

export function getPosts(){
  const posts = api.get(`/api/post`)

  const data = posts.then((response) => response.data)
    .catch((error => error.toJSON().status))

  return data
}

export function getImage(image_name){
  const image = api.get(`/api/get_image/${image_name}`)

  const data = image.then((response) => response.data)
    .catch((error => error.toJSON().status))

  return data
}



