# Espia-ai APP

Aplicativo Mobile do projeto Espia-Aí

## Criando o Ambiente

Instalando as dependencias Node e JDK
- É recomendado o uso do gerenciador de pacotes [Chocolatey](https://chocolatey.org)
```
choco install -y nodejs-lts openjdk11

```

## Ambiente de desenvolvimento Android
Tutorial completo em [Inciando o ambiente de desenvolvimento](https://reactnative.dev/docs/environment-setup)

1. Instalar o [Android Studio](https://developer.android.com/studio?hl=pt&gclid=CjwKCAiA9tyQBhAIEiwA6tdCrJ4X-5WmctX_FBncs0ihkrIW7zeF4z4waBAmdq_fOH8iqi5_kp0gMxoCnFYQAvD_BwE&gclsrc=aw.ds)
2. Instale o SDK do Android
3. Configure a variável ambiente ANDROID_HOME
4. Adicionar plataform-tools ao path

## Criando uma nova aplicação

Abrir a linha de comando na pasta desejada
```
npx react-native init NomeDoProjeto
cd NomeDoProjeto
```

## Inciando a aplicação
1. Inciar as instâncias
```
npx react-native start
```
2. Iniciar a aplicação no dispositivo
```
npx react-native run-android
```
## Modificando o aplicativo
* Abra o arquivo App.js em qualquer editor de texto desejado e modifique algumas linhas
* Aperte a tecla R duas vezes para recarregar a aplicação
